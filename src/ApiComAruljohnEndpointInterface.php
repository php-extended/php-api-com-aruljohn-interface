<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-aruljohn-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComAruljohn;

use Iterator;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiComAruljohnEndpointInterface class file.
 * 
 * This class is the interpreter of the data at the https://aruljohn.com website.
 * 
 * @author Anastaszor
 */
interface ApiComAruljohnEndpointInterface extends Stringable
{
	
	/**
	 * Retrieves the user agent list from the database.
	 * 
	 * @return Iterator<string>
	 * @throws ClientExceptionInterface
	 * @throws RuntimeException
	 */
	public function getUserAgentIterator() : Iterator;
	
}
